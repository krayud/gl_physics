package libs;

import org.lwjgl.Sys;

public class DeltaTimer
{
	private  long lastFrame;
	private  int	deltaTime;
	
	private  long getTime()
	{
		return Sys.getTime();
	}
	
	public  int Update()
	{
		long currentTime = getTime();
		int delta = (int)(currentTime - this.lastFrame);
		lastFrame = getTime();
		deltaTime = delta;
		return deltaTime;
	}
	
	public  void Init()
	{
		lastFrame = getTime();
	}
	
	public  int GetDelta()
	{
		return deltaTime;
	}

}
