package libs;

import static org.lwjgl.opengl.GL11.*;


public class Vector2{
	
	private float x;
	private float y;
	
	public float getX(){
		return this.x;
	}
	public float getY(){
		return this.y;
	}

	public Vector2(float x, float y){
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Копирование вектора
	 * @return Новый вектор, равный тому, у которого вызван метод
	 */
	public Vector2 Copy()
	{
		return new Vector2(this.getX(), this.getY());
	}
	
	/**
	 * Сложение векторов
	 * @param other вектор для сложения
	 * @return Новый вектор
	 */
	public Vector2 Add(Vector2 other){
		float x = this.x + other.getX();
		float y = this.y + other.getY();
		return new Vector2(x, y);
	}
	
	/**
	 * Создает новый вектор, направленный в противоположенную сторону
	 * @return Новый вектор
	 */
	public Vector2 Reverse(){
		Vector2 v = this.Copy();
		v.MulOnScalar(-1f);
		return v;
	}
	
	/**
	 * Вычитает вектор other из текущего вектора
	 * @param other
	 * @return Новый вектор
	 */
	public Vector2 Sub(Vector2 other){
		float x = this.x - other.getX();
		float y = this.y - other.getY();
		return new Vector2(x, y);
	}
	
	/**
	 * Скалярное произведение векторов
	 * @param other вектор
	 * @return Скалярное произведение векторов (число float)
	 */
	public float ScalarMul(Vector2 other){
		return (this.x * other.getX() + this.y * other.getY());
	}
	
	/**
	 * Умножение на скаляр
	 * @param scalar число, на которое умножается вектор
	 * @return Этот же вектор
	 */
	public Vector2 MulOnScalar(float scalar){
		this.x *= scalar;
		this.y *= scalar;
		return this;
	}
	
	/**
	 * Деление на скаляр
	 * @param scalar число
	 * @return Этот же вектор
	 */
	public Vector2 DivOnScalar(float scalar){
		this.x /= scalar;
		this.y /= scalar;
		return this;
	}
	
	/**
	 * Длина вектора
	 * @return Число (float)
	 */
	public float Length(){
		return (float)Math.sqrt(this.x * this.x + this.y * this.y);
	}
	
	/**
	 * Нормальный вектор (единичный)
	 * @return Новый вектор
	 */
	public Vector2 Normalize(){
		float len = this.Length();
		float x = this.x / len;
		float y = this.y / len;
		return new Vector2(x, y);
	}
	
	/**
	 * Обнуление компонент вектора
	 */
		public void FillZero(){
			this.x = 0;
			this.y = 0;
		}

	/**
	 * Отрисовка вектора.
	 * @param red красная компонента
	 * @param green зеленая компонента
	 * @param blue синяя компонента
	 * @param fromX X координата точки 
	 * @param fromY Y координата точки
	 */
	public void Draw(float red, float green, float blue, float fromX, float fromY){
		glColor3f(red, green, blue);
		glBegin(GL_LINES);
			glVertex2f(fromX, fromY);
			glVertex2f(fromX + this.x, fromY + this.y);
		glEnd();
	}
	/**
	 * Отрисовка вектора.
	 * @param red красная компонента
	 * @param green зеленая компонента
	 * @param blue синяя компонента
	 * @param from вектор, указывающий начальную точку отрисовки
	 */
	public void Draw(float red, float green, float blue, Vector2 from){
		float fromX = from.getX();
		float fromY = from.getY();
		
		glColor3f(red, green, blue);
		glBegin(GL_LINES);
			glVertex2f(fromX, fromY);
			glVertex2f(fromX + this.x, fromY + this.y);
		glEnd();
	}
}

