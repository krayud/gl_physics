package objects;


import org.lwjgl.input.Keyboard;

import core.Game;
import libs.SimpleText;
import libs.Vector2;
import static org.lwjgl.opengl.GL11.*;

public class Car
{
	//Скалярные
	private Game  game;				// Объект классы игры
	private float sideLength;		// Длина стороны
	private float mass;				// Масса
	private float frictionRation;	// Коэф. трения самого предмета
	private float gravity;			// Гравитация
	
	//Векторный
	private Vector2 centerMass;		// Центр масс
	private Vector2 velocity;		// Скорость
	private Vector2 acceleration;	// Ускорение
	private Vector2 force;			// Сила, приложенная к объекту
	private Vector2 friction;		// Сила трения
	private Vector2 weight;			// Вес
	
	Vector2 rigth	= new Vector2(14f, 0);
	Vector2 up		= new Vector2(0, 14f);
	Vector2 accelerationForce = new Vector2(0, 0);
	
	public Car(float mass, Vector2 centerMass, Game game)
	{
		this.game			= game;
		this.mass			= mass;
		this.centerMass		= centerMass;
		this.sideLength		= 5;
		this.frictionRation	= 0.05f;
		this.gravity		= 0.0005f;
		
		this.friction		= new Vector2(0, 0);
		this.velocity		= new Vector2(0, 0);
		this.acceleration	= new Vector2(0, 0);
		this.force			= new Vector2(0, 0);
		this.weight			= new Vector2(0, this.mass * this.gravity);
	}
	
	public void SetForce(Vector2 force)
	{
		this.force = force;
	}
	
	public void Input()
	{
		accelerationForce.FillZero();
		
		if(Keyboard.isKeyDown(Keyboard.KEY_UP))
			accelerationForce = accelerationForce.Add(up);
		else 
			if(Keyboard.isKeyDown(Keyboard.KEY_DOWN))
				accelerationForce = accelerationForce.Add(up.Reverse());

		if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT))
			accelerationForce = accelerationForce.Add(rigth);
		else 
			if(Keyboard.isKeyDown(Keyboard.KEY_LEFT))
				accelerationForce = accelerationForce.Add(rigth.Reverse());
	}
	
	public void Update(int dTime)
	{
		this.SetForce(accelerationForce);
		
		if( (Math.abs(force.Length()) > 0) || (Math.abs(velocity.Length()) > 0.025) )
		{
			
			
			
			/**/
			acceleration	= force.Copy().DivOnScalar(this.mass);			// F=ma
			velocity		= velocity.Add(acceleration).Add(friction); 	// V=V0+a
			friction		= velocity.Reverse().Normalize();
			friction		= friction.MulOnScalar( weight.Length() * frictionRation ); // Fтр=uWeight
			centerMass		= centerMass.Add(velocity);
			/**/
		}
		
		this.PrintCarInfo();
	}
	
	private void PrintCarInfo()
	{
		glColor3f(100, 100, 100);
		SimpleText.drawString("Force: "+force.Length(), 5, game.GetWinH() - 15);
		SimpleText.drawString("Acceleration: "+acceleration.Length(), 5, game.GetWinH() - 30);
		SimpleText.drawString("Velocity: "+velocity.Length(), 5, game.GetWinH() - 45);
		SimpleText.drawString("Friction: "+friction.Length(), 5, game.GetWinH() - 60);
	}
	
	public void DrawForces()
	{
		Vector2 velocityForDraw			= velocity.Copy().MulOnScalar(10);
		Vector2 accelerationForDraw		= acceleration.Copy().MulOnScalar(500);	
		Vector2	frictionForDraw			= friction.Copy().MulOnScalar(4000);
		
		accelerationForDraw.Draw(100, 0, 0, this.centerMass);
		velocityForDraw.Draw(0, 100, 0, this.centerMass);
		frictionForDraw.Draw(0, 0, 100, this.centerMass);
		
	}
	
	public void Draw()
	{
		glColor3f(100, 0 , 100);
		glRectf(centerMass.getX() - sideLength, centerMass.getY() - sideLength, centerMass.getX() + sideLength, centerMass.getY() + sideLength);
		this.DrawForces();
	}
}

