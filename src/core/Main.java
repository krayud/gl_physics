package core;

import core.Game;

public class Main
{
	public static void main(String[] args)
	{
		Game game = new Game();
		game.CreateWindow();
		game.InitGL();
		game.InitObjects();
		game.GameLoop();
	}

}
