package core;

import static org.lwjgl.opengl.GL11.*;
import libs.SimpleText;
import libs.DeltaTimer;
import libs.Vector2;
import objects.Car;

import org.lwjgl.opengl.*;
import org.lwjgl.*;

public class Game
{
	private int WINDOW_W 	= 640;
	private int WINDOW_H 	= 480;
	private DeltaTimer deltaTimer	= new DeltaTimer();
	
	public int GetWinW(){ return WINDOW_W; }
	public int GetWinH(){ return WINDOW_H; }
	
	Car car;
	
	public void InitObjects()
	{
		car = new Car(200, new Vector2(100, 100), this);
	}
	
	public void CreateWindow()
	{
		try
		{
			Display.setDisplayMode(new DisplayMode(WINDOW_W, WINDOW_H));
			Display.setTitle("OpenGL Physics");
			Display.create();
		} 
		catch(LWJGLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void InitGL()
	{
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, WINDOW_W, 0, WINDOW_H, 1, -1);
		glMatrixMode(GL_MODELVIEW);
	}
	
	public void GameLoop()
	{
		this.deltaTimer.Init();
		
		while(!Display.isCloseRequested())
		{
			glClearColor(0, 0, 0, 0);
			glClear(GL_COLOR_BUFFER_BIT);
			
			this.Input();
			this.Update();
			this.Draw();
			
			Display.update();
			Display.sync(60);
		}
		Display.destroy();
	}

	private void Input()
	{
		car.Input();
	}
	
	private void Update()
	{
		this.deltaTimer.Update();
		car.Update(this.deltaTimer.GetDelta());
	}
	
	private void Draw()
	{
		SimpleText.drawString("Ping: "+this.deltaTimer.GetDelta()+"ms", this.WINDOW_W - 150, this.WINDOW_H - 15);
		car.Draw();
	}
	
}
